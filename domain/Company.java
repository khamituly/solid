package com.company.domain;

import com.company.Entities.Employee;
import com.company.Entities.OficePlanktons.JavaDevolper;
import com.company.Entities.OficePlanktons.JavaScriptDeveloper;
import com.company.Entities.OficePlanktons.Manager;
import com.company.Entities.OficePlanktons.PythonDeveloper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Company {
    private List<Employee> employeesList;

    public void Extract() throws FileNotFoundException {
        File file = new File("C:\\Users\\Khamituly\\IdeaProjects\\Solid\\src\\com\\company\\db.txt");
        Scanner sc = new Scanner(file);

        int index,experience;
        String firstname,lastname;
        while(sc.hasNext())

        {
            index = sc.nextInt();
            firstname = sc.next();
            lastname = sc.next();
            experience = sc.nextInt();
            switch (index){
                case 1:
                    Employee emp = new JavaDevolper(firstname,lastname,experience);
                    employeesList.add(emp);
                    break;
                case 2:
                    Employee emp1 = new JavaScriptDeveloper(firstname,lastname,experience);
                    employeesList.add(emp1);
                    break;
                case 3:
                    Employee emp2 = new PythonDeveloper(firstname,lastname,experience);
                    employeesList.add(emp2);
                    break;
                case 4:
                    Employee emp3 = new Manager(firstname,lastname,experience);
                    employeesList.add(emp3);
                    break;
                default:
                    break;
            }
        }
    }

    public void create(Request request) throws FileNotFoundException {
        Extract();
        switch (request){
            case WebApp:
                WebAppCreation();
                break;
            case WebPage:
                WebPageCreation();
                break;
            case App:
                AppCreation();
                break;
            case SiteLayout:
                SiteLayoutCreation();
                break;
            default:
                System.out.println("Error!");
                break;
        }
    }

    public void WebAppCreation(){

        PythonDeveloper pythonDev = new PythonDeveloper();
        JavaDevolper javaDev = new JavaDevolper();
        Manager mg = new Manager();
        for(int i=0; i<employeesList.size(); i++){
            if(employeesList.get(i) instanceof PythonDeveloper){
                pythonDev = (PythonDeveloper) employeesList.get(i);
            }else if(employeesList.get(i) instanceof  JavaDevolper){
                javaDev = (JavaDevolper) employeesList.get(i);
            }
            else if(employeesList.get(i) instanceof Manager){
                mg = (Manager) employeesList.get(i);
            }
        }

        int total = pythonDev.cost()+mg.cost()+javaDev.cost();
        System.out.println("Total price for product: "+total);
    }

    public void WebPageCreation(){
        PythonDeveloper dev = new PythonDeveloper();
        Manager mg = new Manager();
        for(int i=0; i<employeesList.size(); i++){
            if(employeesList.get(i) instanceof PythonDeveloper){
                dev = (PythonDeveloper) employeesList.get(i);
            }else if(employeesList.get(i) instanceof Manager){
                mg = (Manager) employeesList.get(i);
            }
        }
        int total = dev.cost()+mg.cost();
        System.out.println("Total price for product: "+total);
    }

    public void SiteLayoutCreation(){
        JavaScriptDeveloper dev = new JavaScriptDeveloper();
        Manager mg =new Manager();
        for(int i=0; i<employeesList.size(); i++){
            if(employeesList.get(i) instanceof JavaScriptDeveloper){
                dev = (JavaScriptDeveloper) employeesList.get(i);
            }else if(employeesList.get(i) instanceof Manager){
                mg = (Manager) employeesList.get(i);
            }
        }
        int total = dev.cost()+mg.cost();
        System.out.println("Total price for product: "+total);
    }

    public void AppCreation(){
        JavaScriptDeveloper javasDev = new JavaScriptDeveloper();
        PythonDeveloper pythonDev = new PythonDeveloper();
        JavaDevolper javaDev =new JavaDevolper();
        Manager mg = new Manager();
        for(int i=0; i<employeesList.size(); i++) {
            if (employeesList.get(i) instanceof PythonDeveloper) {
                pythonDev = (PythonDeveloper) employeesList.get(i);
            } else if (employeesList.get(i) instanceof Manager) {
                mg = (Manager) employeesList.get(i);
            } else if (employeesList.get(i) instanceof JavaDevolper) {
                javaDev = (JavaDevolper) employeesList.get(i);
            } else if (employeesList.get(i) instanceof JavaScriptDeveloper) {
                javasDev = (JavaScriptDeveloper) employeesList.get(i);
            }
        }

        int total = pythonDev.cost()+mg.cost()+javaDev.cost()+javasDev.cost();
        System.out.println("Total price for product: "+total);
    }

}
