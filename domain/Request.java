package com.company.domain;

public enum Request {
    WebPage,
    WebApp,
    App,
    SiteLayout
}
