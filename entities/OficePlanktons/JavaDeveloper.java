package com.company.Entities.OficePlanktons;

import com.company.Entities.Employee;
import com.company.interfaces.IBackDeveloper;

public class JavaDevolper extends Employee implements IBackDeveloper {
    private static int salary = 120000;

    public JavaDevolper(){}

    JavaDevolper(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public JavaDevolper(String firstname, String lastname, int experience) {
        super(firstname, lastname, experience);
    }


    @Override
    public void work() {
        System.out.println("Writing backend code...");
    }

    @Override
    public void profession() {
        System.out.println("I am a back-end Java developer");
    }

    @Override
    public int cost() {
        return ((salary*((getExperience()/100)*2))/100)+salary;
    }
}
