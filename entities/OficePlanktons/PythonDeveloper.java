package com.company.Entities.OficePlanktons;

import com.company.Entities.Employee;
import com.company.interfaces.IBackDeveloper;
import com.company.interfaces.IFrontDeveloper;

public class PythonDeveloper extends Employee implements IBackDeveloper, IFrontDeveloper {
    private static int salary = 200000;

    public PythonDeveloper(){
        super();
    }

    PythonDeveloper(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public PythonDeveloper(String firstname, String lastname, int experience) {
        super(firstname, lastname, experience);
    }

    @Override
    public void work() {
        System.out.println("coding...");
    }

    @Override
    public void profession() {
        System.out.println("I am fullstack developer");
    }

    @Override
    public int cost() {
        return ((salary*((getExperience()/100)*2))/100)+salary;
    }
}
