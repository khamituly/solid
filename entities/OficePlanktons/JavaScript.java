package com.company.Entities.OficePlanktons;

import com.company.Entities.Employee;
import com.company.interfaces.IFrontDeveloper;

public class JavaScriptDeveloper extends Employee implements IFrontDeveloper {
    private static int salary = 120000;

    public JavaScriptDeveloper(){}
    public JavaScriptDeveloper(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public JavaScriptDeveloper(String firstname, String lastname, int experience) {
        super(firstname, lastname, experience);
    }

    @Override
    public void work() {
        System.out.println("writing smth...");
    }

    @Override
    public void profession() {
        System.out.println("I am a front-end developer");
    }

    @Override
    public int cost() {
       return  ((salary*((getExperience()/100)*2))/100)+salary;
    }
}
