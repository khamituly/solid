package com.company.Entities.OficePlanktons;

import com.company.Entities.Employee;

public class Manager extends Employee {
    private static int salary = 100000;

   public Manager(){}

    public Manager(String firstname, String lastname, int experience) {
        super(firstname, lastname, experience);
    }

    public Manager(String firstname, String lastname) {
        super(firstname, lastname);
    }

    @Override
    public void work() {
        System.out.println("I do my management work");
    }

    @Override
    public void profession() {
        System.out.println("I am a manager");
    }

    @Override
    public int cost() {
        return ((salary*((getExperience()/100)*2))/100)+salary;
    }


}
