package com.company.Entities;

import com.company.interfaces.IEmployee;

public abstract class Employee extends Person implements IEmployee {

    private int experience;



    public Employee(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public Employee(String firstname, String lastname, int experience) {
        super(firstname, lastname);
        setExperience(experience);
    }

    public Employee() {

    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public abstract int cost();

    @Override
    public void getType(){
        System.out.println(super.getFirstname()+" is a employee");
    }
}
