package com.company.Entities;

import com.company.domain.Company;
import com.company.domain.Request;

import java.io.FileNotFoundException;

public class Client extends Person {

    public Client(){}

    Client(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public void sendRequest(Request request) throws FileNotFoundException {
        Company comp = new Company();
        comp.create(request);
    }

    @Override
    public void getType() {
        System.out.println(super.getFirstname()+" is a client");
    }
}
