package com.company.Entities;

public abstract class Person {
 private String firstname;
 private String lastname;

    Person(){}

    Person(String firstname, String lastname){
    setFirstname(firstname);
    setLastname(lastname);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public abstract void getType();

    @Override
    public String toString() {
        return firstname+lastname;
    }
}
