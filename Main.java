package com.company;

import com.company.Entities.Client;
import com.company.Entities.Person;
import com.company.domain.Request;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Client client = new Client();
        client.sendRequest(Request.App);
    }
}
