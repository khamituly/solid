package com.company.interfaces;

public interface IFrontDeveloper extends IEmployee {
    @Override
    void profession();

    @Override
    void work();
}
