package com.company.interfaces;

public interface IBackDeveloper extends IEmployee {

    @Override
    void work();

    @Override
    void profession();
}
